package dev.bezerra.comp_dispositivos_moveis_03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private TextView nome;
    private TextView idade;
    private TextView timeFutebol;
    private Button sortear;

    private String[] nomes = {
            "Matheus",
            "Juliana",
            "Rodrigo",
            "Gabriela",
            "Pedro",
            "Sabrina"
    };

    private String[] idades = {
            "18 anos",
            "25 anos",
            "21 anos",
            "29 anos",
            "32 anos",
            "27 anos"
    };

    private String[] times = {
            "Avaí",
            "Vasco",
            "Palmeiras",
            "Grêmio",
            "Corinthians",
            "Santos"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nome = (TextView) findViewById(R.id.nome);
        idade = (TextView) findViewById(R.id.idade);
        timeFutebol = (TextView) findViewById(R.id.timeFutebol);
        sortear = (Button) findViewById(R.id.btnSortear);

        sortear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random aleatorio = new Random();

                int indexNomeAleatorio = aleatorio.nextInt(nomes.length);
                int indexIdadeAleatoria = aleatorio.nextInt(idades.length);
                int indexTimeAleatorio = aleatorio.nextInt(times.length);

                nome.setText(nomes[indexNomeAleatorio]);
                idade.setText(idades[indexIdadeAleatoria]);
                timeFutebol.setText(times[indexTimeAleatorio]);
            }
        });
    }
}
