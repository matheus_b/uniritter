package com.uniritter.aulasqlite2510;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
//           1) Criar um banco de dados doadores;
            SQLiteDatabase bancoDados = openOrCreateDatabase("doadores", MODE_PRIVATE, null);

//           2) nome de tabela a definir;
//           3) campos obrigatórios: nome, idade, tiposangue, telefone e apto;
            bancoDados.execSQL("CREATE TABLE IF NOT EXISTS pessoas(nome VARCHAR, idade INT(3), tipoSangue VARCHAR, telefone VARCHAR, apto VARCHAR)");

//           4) inserir 10 registros;
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Matheus', 30,  'A+',  '51987654321', 'Não')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Mariana', 22,  'B+',  '51998877665', 'Sim')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Adriano', 48,  'O-',  '51923456789', 'Não')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Carlos',  19,  'A+',  '51996633225', 'Não')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Julia',   26,  'O-',  '51987654321', 'Não')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Pâmela',  32,  'A+',  '51998811000', 'Sim')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Ricardo', 29,  'A+',  '51995511334', 'Sim')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Bruna',   35,  'B+',  '51965489521', 'Sim')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Denis',   48,  'A+',  '51987654321', 'Não')");
            bancoDados.execSQL("INSERT INTO pessoas (nome, idade, tipoSangue, telefone, apto) VALUES ('Lucas',   39,  'O-',  '51943871698', 'Não')");

            Cursor cursor = bancoDados.rawQuery("SELECT nome, idade, tipoSangue, telefone, apto FROM pessoas", null);

//            5) Realizar update em 04 registros a escolher;
//            Cursor cursor = bancoDados.rawQuery("UPDATE pessoas SET apto = 'Sim' WHERE nome='Matheus'", null);
//            Cursor cursor = bancoDados.rawQuery("UPDATE pessoas SET idade = 25 WHERE nome='Mariana'", null);
//            Cursor cursor = bancoDados.rawQuery("UPDATE pessoas SET telefone = '51976424079' WHERE nome='Denis'", null);
//            Cursor cursor = bancoDados.rawQuery("UPDATE pessoas SET apto = 'Sim' WHERE nome='Carlos'", null);

//            6) Deletar 02 registros a escolher;
//            Cursor cursor = bancoDados.rawQuery("DELETE FROM pessoas WHERE nome='Ricardo' ", null);
//            Cursor cursor = bancoDados.rawQuery("DELETE FROM pessoas WHERE nome='Lucas' ", null);

//            7) Visualizar doadores aptos somente;
//            Cursor cursor = bancoDados.rawQuery("SELECT nome, idade, tipoSangue, telefone, apto FROM pessoas WHERE apto == 'Sim'", null);

            int indiceColunaNome = cursor.getColumnIndex("nome");
            int indiceColunaIdade = cursor.getColumnIndex("idade");
            int indiceColunaTipoSangue = cursor.getColumnIndex("tipoSangue");
            int indiceColunaTelefone = cursor.getColumnIndex("telefone");
            int indiceColunaApto = cursor.getColumnIndex("apto");

            cursor.moveToFirst();

            while (cursor != null) {

                Log.i("RESULTADO - nome: ", cursor.getString(indiceColunaNome));
                Log.i("RESULTADO - idade: ", cursor.getString(indiceColunaIdade));
                Log.i("RESULTADO - sangue: ", cursor.getString(indiceColunaTipoSangue));
                Log.i("RESULTADO - telefone: ", cursor.getString(indiceColunaTelefone));
                Log.i("RESULTADO - apto: ", cursor.getString(indiceColunaApto));

                cursor.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}