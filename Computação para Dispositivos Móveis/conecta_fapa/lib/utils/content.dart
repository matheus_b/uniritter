import 'package:conectafapa/models/category.dart';
import 'package:conectafapa/models/category_store.dart';
import 'package:conectafapa/models/post.dart';
import 'package:conectafapa/models/store.dart';

categories() {
  List<Category> categories = [
    Category(
      0,
      'Todos',
      null,
      './assets/icons/todos.png',
    ),
    Category(
      1,
      'Alongamento',
      './assets/img/categories/alongamento.jpg',
      './assets/icons/alongamento.png',
    ),
    Category(
      2,
      'Saúde Mental',
      './assets/img/categories/saude-mental.jpg',
      './assets/icons/saude-mental.png',
    ),
    Category(
      3,
      'Bem-estar',
      './assets/img/categories/bem-estar.jpeg',
      './assets/icons/bem-estar.png',
    ),
    Category(
      4,
      'Dicas',
      './assets/img/categories/dicas.jpg',
      './assets/icons/dicas.png',
    ),
    Category(
      5,
      'Promoções',
      './assets/img/categories/promocoes.jpeg',
      './assets/icons/promocoes.png',
    ),
    Category(
      6,
      'Trânsito',
      './assets/img/categories/transito.png',
      './assets/icons/transito.png',
    ),
    Category(
      7,
      'Cidadania',
      './assets/img/categories/cidadania.jpg',
      './assets/icons/cidadania.png',
    ),
    Category(
      8,
      'Notícias',
      './assets/img/categories/noticias.jpg',
      './assets/icons/noticias.ico',
    ),
  ];
  return categories;
}

posts() {
  List<Post> posts = [
    Post(
        1,
        'Alongamento: seis dicas para fazer da forma correta',
        'https://www.bonde.com.br/img/bondenews/2019/01/img_1_27_4543.jpg',
        '03/10/2019',
        'Muitas vezes, durante a musculação, as pessoas não estão fazendo o movimento errado, mas, a flexibilidade ruim, '
            'por causa da falta de alongamento, atrapalha a execução da atividade. Por isso, se você quer relaxar e fazer '
            'seu treino render mais, seguem algumas dicas para executar o alongar corretamente. <br><br> <b>1 - Alongue '
            'antes da musculação ou do treino aeróbico</b><br> Muita gente aprendeu que o alongamento deve ser realizado '
            'após as atividades físicas, mas não precisa ser sempre assim. Se os exercícios físicos forem intensos, o melhor '
            'é descansar em seguida, deixando de lado, inclusive o alongamento. "Isso porque, a musculatura já foi estressada, '
            'ou seja, submetida a algum tipo de esforço", adverte o fisiologista Paulo Correia. O alongamento antes da musculação '
            'ou dos exercícios aeróbicos é recomendado como forma de preparar o corpo para movimentos mais amplos e também funciona '
            'como aquecimento. <br><br><b>2 - Comece a alongar antes de dormir</b><br>Um dos melhores momentos para fazer '
            'alongamento é minutos antes da hora de dormir. Os movimentos devem ser executados de forma passiva e tranquila. '
            'A execução dos movimentos para alongar é uma forma eficaz de repousar com o corpo mais relaxado e de ter um sono '
            'de maior qualidade.<br><br><b>3 - Invista na aula de alongamento</b><br>Há tempos o alongamento é considerado uma '
            'modalidade e ganhou status de aula na academia. A aula usa exercícios que envolvem a mobilidade das articulações, '
            'aumentando a flexibilidade. Ela pode durar entre 30 minutos e uma hora. O fisiologista Paulo Correia ressalta que '
            'as técnicas para alongar variam muito, podendo ser movimentos em que você mantém a posição por cinco ou dez segundos '
            'ou movimentos em que você fica entre dez e 40 segundos mantendo a posição, depois retorna a posição inicial. '
            'Ainda tem a técnica de insistência, que visa burlar o "fuso" muscular. É feito o alongamento forçando a posição até '
            'aumentar a flexibilidade e amplitude dos movimentos. Outra forma é a de rompimento dos blocos musculares, utilizada '
            'por bailarinos, em que você fica minutos (chegando até a horas!) numa posição de alongamento, fazendo uma insistência '
            'mais agressiva.<br><br><b>4- Respire direito e cuidado com a coluna enquanto alonga</b><br>A respiração é um fator '
            'importante para oxigenação do corpo enquanto você executa os movimentos. O ideal é expirar durante o movimento de '
            'alongamento, pois isso ajuda a relaxar. E como fica a coluna nessa atividade? O fisiologista do esporte Paulo '
            'Correia adverte apenas que a postura não pode ser desconfortável.<br><br><b>5- Comece pelos músculos primários!</b><br>'
            'Existe uma ordem que deve ser respeitada para atingir melhores resultados. Sempre comece pelos músculos primários '
            '(eles não têm esse nome à toa!), pois eles são os que participam mais intensamente da execução dos movimentos. Eles '
            'compreendem toda a musculatura superior das costas, peitoral, posterior das coxas e glúteos. Em seguida, movimente '
            'os músculos secundários, aqueles que auxiliam na movimentação, como bíceps e os músculos que envolvem os dedos e '
            'as mãos.<br><br><b>6- Não trabalhe a flexibilidade sem auxílio profissional</b><br>O primeiro erro de quem vai '
            'alongar é trabalhar a sua flexibilidade sem nem um tipo de avaliação profissional. Além disso, o acompanhamento '
            'de um profissional para ensinar e auxiliar a execução dos exercícios é primordial. Depois que você já souber o '
            'passo a passo, pode repetir os movimentos em casa.',
        categories()[1]),
    Post(
        2,
        'Pesquisa mostra efeito do exercício físico no combate à depressão',
        'https://img.buzzfeed.com/buzzfeed-static/static/2018-08/9/17/asset/buzzfeed-prod-web-01/sub-buzz-1740-1533850427-1.jpg',
        '03/10/2019',
        'Há muito tempo se sabe que exercícios podem fazer você se sentir melhor – tanto fisica como mentalmente – e que isso pode ter um efeito muito benéfico em casos de depressão.<br><br>Agora, um grande estudo confirmou que mesmo só um pouco de atividade física pode melhorar o humor se comparado a não fazer absolutamente nada. Além disso, alguns tipos de exercícios podem ser mais eficazes do que outros.<br><br>Qualquer tipo de exercício — incluindo caminhar ou fazer trabalhos domésticos — ajudou a reduzir esse número até uma média de 1,5 dias por mês (ou 43%). Esportes coletivos, ciclismo, exercícios aeróbicos e exercícios na academia tiveram o maior efeito, reduzindo os dias de problemas de saúde mental em cerca de 20%. Caminhar, por outro lado, esteve relacionado com uma redução de 10%.<br><br>Pessoas que se exercitavam por 30 a 60 minutos por sessão e 3 a 5 dias por semana pareceram ter os maiores benefícios se comparados àqueles que se exercitavam menos ou mais.<br><br>Na realidade, pessoas que se exercitavam 23 vezes no mês ou por mais de 90 minutos por sessão apresentaram pior saúde mental do que aqueles que se exercitaram menos vezes ou por períodos mais curtos de tempo, observaram os autores.<br><br>No geral, 45 minutos era mais benéfico do que se exercitar por menos tempo e não foi observado nenhum benefício em se exercitar por mais do que uma hora.<br><br>"Não é que todos tenham que sair e correr uma maratona. Na realidade, correr nem foi o mais eficaz", disse o pesquisador que liderou o estudo, Adam Chekroud, ao BuzzFeed News. "Coisas como ioga, caminhar e até mesmo atividades domésticas já traziam benefícios se comparadas a não fazer nada<br><br>O efeito era ainda maior em pessoas que disseram ter sido diagnosticadas com depressão. Entre esse grupo, aqueles que se exercitavam relataram 7,1 dias por mês em que sua saúde mental estava "ruim" comparado a 10,9 dias por aqueles que não tinham feito exercícios físicos.".',
        categories()[2]),
    Post(
        3,
        'Seus posts no Instagram podem dizer muito sobre sua saúde mental',
        'https://conteudo.imguol.com.br/c/noticias/d9/2017/08/09/recurso-instagram-stories-1502307469863_615x300.jpg',
        '03/10/2019',
        'As fotos que você compartilha online dizem muito. Elas podem servir como uma forma de autoexpressão ou um diário de viagem. Podem refletir seu estilo e suas peculiaridades. Mas podem transmitir ainda mais do que você imagina: as fotos que você compartilha online podem conter pistas sobre sua saúde mental, sugere uma nova pesquisa.<br><br>Das cores e rostos em suas fotos até os retoques feitos por eles antes de serem postadas, os usuários do Instagram com um histórico de depressão parecem apresentar o mundo de modo diferente de seus pares, segundo o estudo, publicado nesta semana na revista EPJ Data Science.<br><br>"As pessoas em nossa amostragem que estavam com depressão tendiam a postar fotos que, em uma base pixel por pixel, tendiam a ser mais azuis, mais escuras e mais cinzentas em média do que as de uma pessoa sã", disse Andrew Reece, um pesquisador de pós-doutorado da Universidade de Harvard e coautor do estudo juntamente com Christopher Danforth, um professor da Universidade de Vermont.<br><br>A dupla identificou os participantes como "deprimidos" ou "sãos" com base em um diagnóstico clínico de depressão no passado. Eles então usaram ferramentas de aprendizado de máquina para encontrar padrões nas fotos e criar um modelo para predição de depressão por meio das postagens.<br><br>Eles descobriram que os participantes deprimidos usavam menos filtros do Instagram, aqueles que permitem aos usuários alterar digitalmente o brilho e cores das fotos antes de serem postadas. Quando esses usuários adicionavam um filtro, eles tendiam a escolher "Inkwell", que retira a cor da foto, deixando-a em preto-e-branco. Os usuários sãos tendiam a preferir "Valencia", que dá mais luminosidade à foto.<br><br>Os participantes deprimidos apresentavam maior probabilidade de postar fotos contendo um rosto. Mas quando participantes sãos postavam fotos com rostos, as deles tendiam a ter um número maior deles, em média.',
        categories()[2]),
    Post(
        4,
        'Conheça os benefícios surpreendentes dos alimentos diuréticos para o seu organismo',
        'https://lar-natural.com.br/laradmin/uploads/2015/04/Alimentos-diur%C3%A9ticos-contra-o-incha%C3%A7o-e-a-press%C3%A3o-alta.jpg',
        '03/10/2019',
        '<b>O que são alimentos diuréticos?</b><br>Os alimentos diuréticos são aqueles que possuem alto teor de água na sua composição, e, por isso, contribuem para hidratação e consequente redução do inchaço do corpo. São também excelentes fontes de vitaminas, minerais e fibras, importantes reguladores do nosso metabolismo.<br><br><b>Quais os benefícios dos alimentos diuréticos?</b><br>O inchaço ou retenção de líquidos podem ser ocasionados por vários fatores, tais como: consumo excessivo de sódio, baixo consumo de potássio, mal funcionamento intestinal, alteração no metabolismo hormonal etc. Para redução desses sintomas, o consumo de alimentos diuréticos, ricos em água, podem contribuir. <br><br>Os alimentos diuréticos são capazes de aumentar a secreção urinária, eliminando a retenção que causa inflamações, já que na urina são expelidas as toxinas que entram no corpo, pelo consumo de alimentos, medicamentos, etc.',
        categories()[3]),
    Post(
        5,
        'O que fazer neste fim de semana em Porto Alegre',
        'https://www.jornaldocomercio.com/_midias/jpg/2019/09/23/joaquin_phoenix_vive_arthur_fleck_em_coringa__foto_niko_tavernise___3_-8850094.jpg',
        '03/10/2019',
        'Neste finde, que vai de sexta a domingo, as dicas têm cinema com estreia de Coringa, música, com shows de Pitt e Paula Toller, mostra de artes em diversos locais, como o Museu Iberê Camargo e no Quarto Distrito.<br><br><b>Cinema:</b> Coringa traz uma super interpretação do ator Joaquin Phoenix. O filme já está nos cinemas em diversas salas na Capital. O filme também está gerando polêmicas. Bom ver para poder entrar na pauta. <br><br><b>Música:</b> Tem as cantoras Pitty, no Pepsi on Stage, e de Paula Toller, no Auditório Araújo Vianna, a partir da 21h, que sobe ao palco com o show Como eu Quero. Os dois shows neste sábado (5).<br><br><b>Artes visuais:</b> Mostra 4 Visões está no átrio da Fundação Iberê Camargo (Padre Cacique, 2.000), com os artistas Cristina Leal, Olga Velho, Silvia Brum e André Santos. A exposição, gratuita, consiste de um recorte de obras sobre possibilidades e variações de processos criativos dentro de narrativas contemporâneas.<br><br><b>Cartum e economia criativa:</b> A amizade de longa data entre o animador Otto Guerra e o cartunista Adão Iturrusgarai - que rendeu trabalhos como, por exemplo, o longa-metragem de animação Rocky & Hudson: os caubóis gays (1994) - será celebrada no Festival Ottorrusgarai.',
        categories()[4]),
    Post(
        6,
        'Bourbon Country: Ingresso para sessão 2D em qualquer dia e horário por R\$9,90',
        'https://www.tcheofertas.com.br/tcheofertas/upload/team/2019/06/Cinema-com-Desconto-Cine-Itau-Bourbon-Country-20190614124553.jpg',
        '03/10/2019',
        'Que tal curtir aquele cineminha por um preço beeeeeeem pequeno? Gostou? Então aproveite agora mesmo! É só adquirir seu cupom no site do Tchê Ofertas e assistir os principais lançamentos do cinema por um preço maravilhoso!<br><br>Em parceria com o Cine Itaú, você vai assistir o melhor do cinema em total conforto e em 8 Salas Stadium com capacidade total de 1989 lugares, acesso para deficientes - rampas e corrimões, ar-condicionado, bilheteria informatizada, bomboniere, rede wi-fi disponível, projetores de última geração, som digital. E pra dar aquele toque a mais, o snack bar oferece produtos variados, desde pipoca e refrigerantes de diferentes tamanhos até guloseimas, como balas e chocolates.<br><br><b>Descrição:</b> Ingresso de cinema para qualquer dia (inclusive fim de semana) e qualquer horário (somente sessões 2D), de R\$30 por R\$9,90.<br><br><b>Data:</b> Use seu cupom até 31/12/2019, em qualquer dia da semana, inclusive final de semana e feriados.<br><br><b>Como comprar:</b> Acesse o site: https://www.tcheofertas.com.br/oferta/lazer/porto-alegre/cine-itau-shopping-bourbon-country/ingresso-cinema-desconto-promocao',
        categories()[5]),
    Post(
        7,
        'Caminhão carregado com botijões de gás tomba na BR-290, na região das ilhas',
        'https://www.rbsdirect.com.br/imagesrc/25400176.jpg?w=700',
        '05/10/2019',
        'Um caminhão Mercedes-Benz, modelo 1313, carregado com 450 botijões de gás, tombou no canteiro central da BR-290, na altura do Km 100, na manhã deste sábado (5). O condutor, Fábio da Silva, 37 anos, afirma ter perdido o controle após passar por uma elevada na rodovia, na ponte sobre o Saco da Alemoa:<br><br>— O trânsito parou, eu freei, tentei tirar para o lado para não bater nos carros, mas acabei descendo o barranco.<br><br>Ele viajava ao lado da esposa, que foi levada ao Hospital de Pronto Socorro de Porto Alegre (HPS), onde passou por exames.<br><br>O veículo trafegava no sentido da região sul do RS para Porto Alegre. Os botijões estavam vazios, e seriam carregados em Canoas, para retornar a Camaquã, cidade em que vive a família do caminhoneiro. <br><br>Ao lado da cabine, bastante destruída, e a carroceria com madeiras quebradas, o motorista se mostrava emocionado ao mensurar os prejuízos.<br><br>— É nosso único caminhão, o sustento da gente. A gente luta tanto, e acontece isso. Mas pelo louvor do Senhor, não ficamos feridos — pondera, com as mãos sobre a cabeça.<br><br>O acidente aconteceu às 9h30min. Até as 11h30min, o caminhão ainda não havia sido retirado.<br><br>Chefe de comunicação social da Polícia Rodoviária Federal (PRF), Cássio Garcez reforça o alerta dos cuidados com a manutenção dos veículos.<br><br>— Ainda mais com chuva, tem que checar os pneus antes de pegar a estrada — alerta.',
        categories()[6]),
    Post(
        8,
        'Mais do que conteúdo, escolas ensinam cidadania',
        'https://img.estadao.com.br/resources/jpg/6/5/1568468600356.jpg',
        '05/10/2019',
        'SÃO PAULO - A escola é fundamental para a transmissão de conteúdo e conhecimento, mas também na formação de indivíduos. Desde 2017, a Base Nacional Comum Curricular (BNCC) destaca projeto de vida como uma das dez competências norteadoras da educação básica no País. Em São Paulo, diversos colégios implementaram programas nesse sentido.<br><br>O Colégio Presbiteriano Mackenzie  já realizava ações que buscavam orientar estudantes em relação ao futuro e à carreira profissional. Em 2018, no entanto, a escola sentiu necessidade de ir além. <br><br>“Percebemos que nossos jovens têm cada vez mais dúvidas”, explica a professora Marcia Regis, diretora pedagógica da instituição. “São muitas opções e às vezes a escola fica tão focada no ensino de disciplinas e conteúdos elementares que não abre espaço para o autoconhecimento.” <br><br>O 3.º ano do ensino médio foi escolhido para dar início ao projeto de vida. Hoje, todos os alunos dessa etapa podem experimentar o programa. As atividades não são obrigatórias, mas cerca 670 estudantes (ou 85% do total) participam. Entre as ações estão Carreira em Debate, que traz profissionais de várias áreas para conversas com os alunos, e o Confemack, que simula conferências da Organização das Nações Unidas (ONU), além de orientações para processos seletivos. <br><br>“Com um coaching, os alunos aprendem ferramentas para o autoconhecimento”, diz Marcia. “Eles passaram a se atentar para as atividades a que se dedicam, recebem ajuda para a escolha profissional e traçam um mapa de suas vidas. Com essas atividades, constroem as próprias trilhas.” <br><br>As atividades são divididas de acordo com as séries escolares. <br><br>“Nos primeiros anos, trabalhamos o desenvolvimento de valores, cuidado, respeito e convivência”, explica Laurindo Cisotto, professor e orientador educacional do colégio. “Com o tempo, isso vai fluindo para as escolhas da vida estudantil e do futuro.” <br><br> O orientador destaca o programa Estágio por um Dia. Nele, alunos do 3.º ano são levados a empresas, onde entram em contato com as profissões na prática. <br><br>“É uma oportunidade única porque ainda é muito difícil aproximar o mundo do trabalho da educação”, diz o orientador. “E isso ajuda os estudantes a conhecerem o mercado, a profissão que pensam e em escolher eles mesmos.”',
        categories()[7]),
    Post(
        9,
        '31 anos de SUS: está na hora de soprar as velas e fazer um pedido!',
        'http://bancariospa.org.br/wp3/wp-content/uploads/2018/01/Saude-SUS.jpg',
        '05/10/2019',
        'No dia 19 de setembro, o Sistema Único de Saúde (SUS) brasileiro atingiu a marca dos seus 31 anos e você, caro leitor, deve estar aí se perguntando o que esses 31 anos de SUS tem a ver com o nosso Legislativo. Pois bem. Deixa eu contar aqui uma breve historinha para vocês.<br><br>A história do SUS remonta ao processo de formulação da nossa Constituição Cidadã promulgada ainda em 1988. Cabe aqui destacar que o processo de elaboração de uma nova Constituição, no entanto, não é algo simples, fácil, prático e rápido. Toda vez que uma nova Constituição é formulada, a responsabilidade e dever de formar e organizar a famosa Assembleia Nacional Constituinte (ANC) recai sobre o Legislativo e seus membros. Composta, sobretudo, por deputados e senadores, toda ANC fica responsável por definir e repensar não só as regras do jogo político como também um elemento fundamental e que muitas vezes nos esquecemos: os direitos sociais.<br><br>Antes da instalação da ANC de 1987, o modelo vigente de acesso à saúde no Brasil estava ligado ao que conceitualmente a Ciência Política chama de “Cidadania Regulada”. Regulada, porque somente trabalhadores com carteira assinada (com a nossa famosa CLT) eram considerados como cidadãos aptos a serem atendidos de forma gratuita (na verdade através de um sistema de cooperação entre indústria e convênios médicos) em hospitais.  Somente a título de curiosidade, caro leitor, às vésperas da ANC a porcentagem da população brasileira que tinha carteira assinada não passava dos 40%. Logo, isso significa afirmar que somente 40% da população brasileira aquela época é que tinha direito a saúde pública.<br><br>Mas vamos focar na nossa questão, ou seja, vamos retomar a ligação existente entre a criação do SUS e o Legislativo. O ponto aqui é que entre o dia 1º de fevereiro de 1987 até 19 de setembro de 1988 algo aconteceu no interior da ANC que permitiu a origem não só do nosso Sistema Único de Saúde – acesso à saúde de forma gratuita para todos –, como também a nossa Constituição de 1988, não à toa adjetivada de “Cidadã”.<br><br>Formada, em sua maioria, por parlamentares considerados como conservadores, não era esperado que a ANC de 1987 elaborasse e aprovasse um sistema único de saúde gratuita para todos, bem como outros direitos sociais com ampla abrangência e cobertura popular. O que os especialistas, a história e os dados nos mostram é que o modo como os trabalhos foram organizados e divididos no interior da ANC no Legislativo possibilitaram que um grupo minoritário de parlamentares considerados como progressistas, fossem capazes de elaborar e aprovar uma agenda ampla de direitos sociais, com grande destaque para o SUS. Pela primeira vez, no Brasil, um direito social tornou-se Universal, ou seja, de acesso a todos.<br><br>De lá para cá, 31 anos se passaram. E podemos apontar mil e um problemas na qualidade da saúde pública no Brasil tais como:<br><br><b>- Falta de médicos:</b> O Conselho Federal de Medicina estima que exista 1 médico para cada 470 pessoas.<br><b>- Falta de leitos:</b> Em muitos hospitais faltam leitos para os pacientes. A situação é ainda mais complicada quando se trata de acesso a UTI (Unidade de Terapia Intensiva).<br><b>- Falta de investimentos financeiros:</b> Em 2018, apenas 3,6% do orçamento do governo federal foi destinado à saúde. A média mundial é de 11,7%.<br><b>- Grande espera para atendimento:</b> Agendar consultas com médicos especialistas pode demorar até meses, mesmo para os pacientes de precisam de atendimento imediato. O mesmo acontece com a marcação de exames.<br><br>Mas ainda que a saúde pública no Brasil enfrente esses mil e um problemas em termos, volto a reforçar, da qualidade da oferta do serviço, é importante destacar o quão democrático, o quão legítimo, o quão correto e de alta qualidade foi o estímulo, o princípio e o trabalho dos nossos legisladores em 1987.<br><br>Na definição clássica de Democracia temos que:  a Democracia é o governo do povo e para o povo, cuja finalidade é a igualdade de oportunidades. Nas Democracias representativas (democracias em que escolhemos nossos representantes) o coração do governo é o Legislativo. É onde mora os representantes do povo, do verdadeiro detentor do poder. É onde a prática da igualdade de oportunidades deveria ser praticada em sua plenitude.  É onde nossos representantes devem olhar democraticamente para aqueles que têm menos oportunidades, menos acesso à recursos, menos acessos a políticas sociais, à serviços básicos, etc e pensar, elaborar e aprovar direitos sociais que deem conta de garantir ao menos a oferta de direitos sociais iguais para todos.<br><br>Finalizo esse texto parabenizando os nossos políticos de 1987 (ano em que eu nem era nascida) que durante a Assembleia Nacional Constituinte conseguiram exercer o papel que um verdadeiro Legislativo deve exercer: o papel de olhar para todos e fornecer as oportunidades de uma vida melhor e mais genuína para aqueles que têm menos. E se tivesse na minha frente um bolo com velinhas para soprar e fazer um pedido em comemoração aos 31 anos de SUS, eu não tenho dúvidas de que meu pedido seria: por mais políticas tais como o SUS. Por um Legislativo que volte a ser composto genuinamente por representantes democráticos. Por cidadãos brasileiros que retirem a mão e os olhos de seus próprios umbigos e saibam olhar para aqueles que ainda hoje, 31 anos depois, ainda são muito, mas muito desiguais em oportunidades.<br><br><b>FONTE:</b> https://politica.estadao.com.br/blogs/legis-ativo/31-anos-de-sus-esta-na-hora-de-soprar-as-velas-e-fazer-um-pedido/',
        categories()[7]),
    Post(
        10,
        'Trump impedirá a entrada de imigrantes que não possam pagar seguro médico',
        'https://s2.glbimg.com/grRT8B2q9eXPOCQNyiesFfncuiU=/0x0:1024x690/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2019/r/U/pm36CWQP6C8fAT9lcpaw/000-1l1275.jpg',
        '05/10/2019',
        'Os Estados Unidos impedirão que os imigrantes entrem se não tiverem seguro médico ou forem incapazes de pagar suas despesas de saúde, de acordo com um documento com a assinatura do presidente Donald Trump divulgado nesta sexta-feira (4).<br><br>O pessoal consular só pode conceder vistos a imigrantes que possam provar que "não irão impor um ônus substancial" ao sistema de saúde dos Estados Unidos, de acordo com este texto.<br><br>"Os imigrantes legais têm três vezes mais chances de não ter seguro de saúde do que os cidadãos dos Estados Unidos", afirmou Trump em anúncio presidencial.<br><br>"Os imigrantes que entram no país não devem incorrer em custos mais altos em nosso sistema de saúde e, portanto, para os contribuintes dos Estados Unidos", acrescenta.<br><br>Trump fez desse tema uma questão fundamental na plataforma de sua campanha presidencial em 2016 com uma política dura contra a imigração.<br><br>Da Casa Branca, ele promoveu reformas da legislação sobre imigração, além de ter solicitado às autoridades que realizassem deportações em massa de pessoas que estavam ilegalmente nos Estados Unidos.<br><br>Na semana passada, o governo americano anunciou um plano para reduzir drasticamente a taxa de admissão de refugiados depois de fechar vários acordos de imigração com a América Central.',
        categories()[8]),
  ];

  return posts;
}

categoriesStores() {
  List<CategoryStore> categoryStore = [
    CategoryStore(1, 'Lancheria'),
    CategoryStore(2, 'Restaurante'),
    CategoryStore(3, 'Loja'),
    CategoryStore(4, 'Beleza'),
    CategoryStore(5, 'Auditório'),
    CategoryStore(6, 'Secretaria'),
    CategoryStore(7, 'Gráfica'),
    CategoryStore(8, 'Xerox'),
    CategoryStore(9, 'Laboratório'),
    CategoryStore(10, 'Cozinha'),
    CategoryStore(11, 'Biblioteca'),
    CategoryStore(12, 'Inovação'),
    CategoryStore(13, 'Atelier'),
  ];
  return categoryStore;
}

stores() {
  List<Store> stores = [
    Store(
        1,
        'Secretaria',
        'Prédio 4',
        'Secretaria acadêmica',
        categoriesStores()[5],
        './assets/img/stores/secretaria.jpg',
        -30.032062,
        -51.122871),
    Store(
        2,
        'Auditório',
        'Prédio 6',
        'Auditório do prédio 6',
        categoriesStores()[4],
        './assets/img/stores/auditorio.jpg',
        -30.032139,
        -51.123409),
    Store(
        3,
        'Auditório',
        'Prédio 4',
        'Auditório do prédio 4',
        categoriesStores()[4],
        './assets/img/stores/auditorio-2.jpg',
        -30.032062,
        -51.122871),
    Store(
        4,
        'Loja de acessórios',
        'Prédio 6',
        'Loja de acessórios no prédio 6, com roupas, cadernos, canetas, etc',
        categoriesStores()[2],
        './assets/img/stores/acessorios.jpg',
        -30.032139,
        -51.123409),
    Store(
        5,
        'Coffee Break',
        'Centro de Convivência',
        'Cafeteria no centro de convivência',
        categoriesStores()[0],
        './assets/img/stores/coffee-break.jpg',
        -30.032611,
        -51.122502),
    Store(
        6,
        'Confraria do Calzone',
        'Centro de Convivência',
        'Lancheria no centro de convivência',
        categoriesStores()[0],
        './assets/img/stores/calzone.jpg',
        -30.032611,
        -51.122502),
    Store(
        7,
        'Salão de Beleza',
        'Centro de Convivência',
        'Salão de beleza no centro de convivência',
        categoriesStores()[3],
        './assets/img/stores/salao.jpg',
        -30.032611,
        -51.122502),
    Store(
        8,
        'Cozinha',
        'Prédio 2',
        'Cozinha do curso de gastronomia',
        categoriesStores()[9],
        './assets/img/stores/cozinha.jpg',
        -30.032275,
        -51.122338),
    Store(
        9,
        'Villandry',
        'Prédio 7',
        'Restaurante no prédio 7',
        categoriesStores()[1],
        './assets/img/stores/restaurante.jpg',
        -30.032434,
        -51.123661),
    Store(
        10,
        'Laboratório de saúde',
        'Prédio 7',
        'Laboratório da área de saúde no prédio 7',
        categoriesStores()[8],
        './assets/img/stores/laboratorio-saude.jpg',
        -30.032434,
        -51.123661),
    Store(
        11,
        'Biblioteca',
        'Prédio 5',
        'Biblioteca do campus FAPA, com salas de estudo',
        categoriesStores()[10],
        './assets/img/stores/biblioteca.jpg',
        -30.032606,
        -51.123079),
    Store(
        12,
        'Xerox',
        'Prédio 3',
        'Xerox, cópias e encadernações no prédio 3',
        categoriesStores()[7],
        './assets/img/stores/xerox.jpg',
        -30.032903,
        -51.122746),
    Store(
        13,
        'Polo de Inovação',
        'Prédio 3',
        'Polo de Empreendedorismo e Inovação no prédio 3',
        categoriesStores()[11],
        './assets/img/stores/hub.jpg',
        -30.032903,
        -51.122746),
    Store(
        14,
        'Laboratório de informática',
        'Prédio 1',
        'Laboratório de informática no prédio 1',
        categoriesStores()[8],
        './assets/img/stores/laboratorio-informatica.jpg',
        -30.033125,
        -51.122357),
    Store(
        15,
        'Gráfica',
        'Centro de Convivência',
        'Gráfica no centro de convivência',
        categoriesStores()[6],
        './assets/img/stores/grafica.jpg',
        -30.032611,
        -51.122502),
    Store(
        16,
        'Atelier',
        'Prédio 1',
        'Atelier de uso livre, desenho, projeto, modelos e moda no prédio 1',
        categoriesStores()[12],
        './assets/img/stores/atelier.jpg',
        -30.033125,
        -51.122357),
  ];
  return stores;
}
