import 'package:conectafapa/screens/feed/feed_screen.dart';
import 'package:conectafapa/screens/login/login_screen.dart';
import 'package:conectafapa/screens/store/store_list_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:conectafapa/theme/app_colors.dart';
import 'package:conectafapa/theme/app_styles.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';

class MenuScreen extends StatefulWidget {
  @override
  State createState() => MenuScreenState();
}

class MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
      SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.40,
                decoration: new BoxDecoration(
                    color: Colors.white, //new Color.fromRGBO(255, 0, 0, 0.0),
                    borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(40.0),
                        bottomRight: const Radius.circular(40.0)))),
            Positioned(
              left: MediaQuery.of(context).size.width * 0.35,
              top: MediaQuery.of(context).size.height * 0.07,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.3,
                height: MediaQuery.of(context).size.width * 0.3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: AspectRatio(
                      aspectRatio: 1.1,
                      child: Image.asset(
                        'assets/img/placeholders/avatar.jpg',
                        fit: BoxFit.cover,
                      )),
                ),
              ),
            ),
            Positioned(
              left: MediaQuery.of(context).size.width * 0.28,
              top: MediaQuery.of(context).size.height * 0.28,
              child: Text(
                'Matheus Bezerra',
                style: TextStyle(
                    fontSize: 24,
                    color: AppColors.sherpaBlue,
                    fontFamily: 'HindMedium'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.40),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.45,
              color: AppColors.whiteSmoke,
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.45,
              child: _menu(),
            )
          ],
        ),
      ),
      selected: 'Menu',
      backgroundColor: AppColors.whiteSmoke,
    );
  }

  _menu() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _menuItem(
              Image.asset('./assets/icons/link.png', width: 24, height: 24),
              'Bezerra.dev'),
          _menuItem(
              Image.asset('./assets/icons/link.png', width: 24, height: 24),
              'Ouvindo.me'),
          _menuItem(
              Image.asset('./assets/icons/post.png', width: 24, height: 24),
              'Posts',
              screen: FeedScreen(0)),
          _menuItem(
              Image.asset('./assets/icons/store.png', width: 24, height: 24),
              'Lojas',
              screen: StoreListScreen()),
          _menuItem(
              Image.asset('./assets/icons/exit.png', width: 24, height: 24),
              'Sair',
              screen: StoreListScreen(),
              logout: true),
        ],
      ),
    );
  }

  _menuItem(Image icon, String title, {Widget screen, bool logout = false}) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.only(
            top: 17,
            bottom: 10,
            left: MediaQuery.of(context).size.width * 0.09,
            right: MediaQuery.of(context).size.width * 0.09),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            icon,
            Padding(
              padding: EdgeInsets.only(left: 20, top: 8),
              child: Text(title, style: AppStyles.defaultTextStyle),
            ),
            Spacer(),
            screen != null
                ? Icon(Icons.chevron_right, color: Colors.red[800])
                : Container()
          ],
        ),
      ),
      onTap: () {
        if (logout) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => LoginScreen()),
              (Route<dynamic> route) => false);
        } else if (screen != null) {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => screen,
            ),
          );
        }
      },
    );
  }
}
