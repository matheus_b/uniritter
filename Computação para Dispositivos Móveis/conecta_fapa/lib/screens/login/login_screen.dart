import 'package:conectafapa/screens/home/home_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:conectafapa/theme/app_colors.dart';
import 'package:conectafapa/theme/app_styles.dart';

class LoginScreen extends StatefulWidget {
  @override
  State createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                _background(),
                Container(
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,
                  child: _form(),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.07),
                    child: Column(
                      children: <Widget>[
                        Image.asset('./assets/icons/logo.png', width: 65),
                        Text(
                          'Conecta FAPA',
                          style: TextStyle(color: AppColors.prussianBlue, fontSize: 32, fontFamily: 'HindMedium', height: 0.8),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _form() {
    return Form(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _formInput('Matrícula', false),
          _formInput('Senha', true),
          _formSubmitButton(),
        ],
      ),
    );
  }

  _formInput(label, isObscure) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 60, vertical: 15),
      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
      decoration: BoxDecoration(
          color: Colors.white,
      ),
      child: TextFormField(
        obscureText: isObscure,
        style: AppStyles.defaultTextStyle,
        decoration: InputDecoration(
          hintText: label,
          hintStyle: AppStyles.defaultTextStyle,
          contentPadding: EdgeInsets.fromLTRB(0, 13, 13, 13),
          border: InputBorder.none,
        ),
      ),
    );
  }

  _formSubmitButton() {
    return SizedBox(
      width: MediaQuery
          .of(context)
          .size
          .width * 0.70,
      child: RaisedButton(
        color: AppColors.pacificBlue,
        elevation: 0,
        highlightElevation: 0,
        splashColor: AppColors.pacificBlue,
        highlightColor: AppColors.pacificBlue,
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        onPressed: () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => HomeScreen()),
                  (Route<dynamic> route) => false);
        },
        child: Text(
          "Entrar",
          style: TextStyle(
            fontWeight: FontWeight.w400,
            color: Colors.white,
            fontSize: 15,
          ),
        ),
      ),
    );
  }

  _background() {
    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(
              './assets/img/fapa.jpeg'),
          fit: BoxFit.cover,
        ),
      ),
      height: MediaQuery
          .of(context)
          .size
          .height,
      alignment: Alignment.center,
      child: new BackdropFilter(
        filter: new ui.ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
        child: new Container(
          decoration: new BoxDecoration(
              color: Colors.black.withOpacity(0.2)),
        ),
      ),
    );
  }
}
