import 'package:conectafapa/screens/login/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:conectafapa/theme/app_colors.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  State createState() => OnboardingScreenState();
}

class OnboardingScreenState extends State<OnboardingScreen> {
  PageController controller = PageController();
  final _currentPageNotifier = ValueNotifier<int>(0);
  AnimationController animationController;
  int currentPage = 0;
  ImageProvider bg1;
  ImageProvider bg2;
  ImageProvider bg3;
  ImageProvider bg4;

  @override
  void didChangeDependencies() async {
    bg1 = AssetImage(
        './assets/img/onboarding/1.jpg');
    bg2 = AssetImage(
        './assets/img/onboarding/2.jpg');
    bg3 = AssetImage(
        './assets/img/onboarding/3.jpg');
    bg4 = AssetImage(
        './assets/img/onboarding/4.jpg');
    await precacheImage(bg1, context);
    await precacheImage(bg2, context);
    await precacheImage(bg3, context);
    await precacheImage(bg4, context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[_slide(), _next(), _slideIndicator()],
          ),
        ),
      ),
    );
  }

  _slide() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
            return true;
          },
          child: PageView.builder(
            itemCount: 4,
            controller: controller,
            onPageChanged: (int index) {
              setState(() {
                _currentPageNotifier.value = index;
                currentPage = index;
              });
            },
            itemBuilder: (context, position) {
              if (position == 0) {
                return _firstPage();
              } else if (position == 1) {
                return _secondPage();
              } else if (position == 2) {
                return _thirdPage();
              } else {
                return _fourthPage();
              }
            },
          ),
        ));
  }

  _firstPage() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
      decoration:
          BoxDecoration(image: DecorationImage(image: bg1, fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          Text('Seja bem-vindo ao',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.085,
                  fontWeight: FontWeight.w400)),
          Text('Conecta FAPA',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: MediaQuery.of(context).size.width * 0.085,
                  fontWeight: FontWeight.w400)),
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
            child: Image.asset(
              './assets/img/onboarding/uniritter.png',
              width: MediaQuery.of(context).size.width * 0.65,
            ),
          )
        ],
      ),
    );
  }

  _secondPage() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.18),
      decoration:
          BoxDecoration(image: DecorationImage(image: bg2, fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          Text(
            'Dicas de saúde',
            style: TextStyle(
                color: Colors.white, fontSize: 27, fontWeight: FontWeight.w700),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.1,
                MediaQuery.of(context).size.height * 0.12,
                MediaQuery.of(context).size.width * 0.1,
                0),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.0485,
                    color: AppColors.whiteSmoke,
                    fontWeight: FontWeight.w300),
                children: <TextSpan>[
                  TextSpan(text: 'No '),
                  TextSpan(
                      text: 'Conecta FAPA',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(
                      text:
                          ' você encontra diversas dicas de saúde exclusivas para alunos da UniRitter no campus FAPA\n'),
                  TextSpan(text: '\n\nSão diversos posts sobre '),
                  TextSpan(
                      text: 'alongamento',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ', '),
                  TextSpan(
                      text: 'saúde mental',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ',\n'),
                  TextSpan(
                      text: 'bem-estar',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' e muito mais!'),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _thirdPage() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.18),
      decoration:
          BoxDecoration(image: DecorationImage(image: bg3, fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          Text(
            'Dicas de gastronomia',
            style: TextStyle(
                color: Colors.white, fontSize: 27, fontWeight: FontWeight.w700),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.1,
                MediaQuery.of(context).size.height * 0.12,
                MediaQuery.of(context).size.width * 0.1,
                0),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.0485,
                    color: AppColors.whiteSmoke,
                    fontWeight: FontWeight.w300),
                children: <TextSpan>[
                  TextSpan(text: 'Veja dicas de '),
                  TextSpan(
                      text: 'onde comer',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' e quais os '),
                  TextSpan(
                      text: 'principais pratos',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' servidos nos restaurantes do campus FAPA\n'),
                  TextSpan(text: '\n\nReceba as promoções do dia e leia '),
                  TextSpan(
                      text: 'reviews',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' dos restaurantes escritos por '),
                  TextSpan(
                      text: 'renomados especialistas',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' em gastronomia'),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _fourthPage() {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.18),
      decoration:
          BoxDecoration(image: DecorationImage(image: bg4, fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          Text(
            'Dicas de lojas e serviços',
            style: TextStyle(
                color: Colors.white, fontSize: 27, fontWeight: FontWeight.w700),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.1,
                MediaQuery.of(context).size.height * 0.12,
                MediaQuery.of(context).size.width * 0.1,
                0),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.0485,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
                children: <TextSpan>[
                  TextSpan(
                      text:
                          'Veja um mapa com as lojas e serviços disponíveis no '),
                  TextSpan(
                      text: 'campus FAPA',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' para os alunos.'),
                  TextSpan(text: '\n\nSão diversas '),
                  TextSpan(
                      text: 'lojas, gráficas e serviços de xerox',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: ' exclusivos\n para os alunos. \n\nTem até '),
                  TextSpan(
                      text: 'salão de beleza!',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _slideIndicator() {
    return Container(
      alignment: Alignment.bottomCenter,
      margin: EdgeInsets.only(bottom: 40),
      child: PageViewIndicator(
        pageIndexNotifier: _currentPageNotifier,
        length: 4,
        indicatorPadding: const EdgeInsets.all(4.0),
        normalBuilder: (animationController, index) => Circle(
          size: 10,
          color: Color.fromRGBO(131, 169, 228, 1),
        ),
        highlightedBuilder: (animationController, index) => ScaleTransition(
          scale: CurvedAnimation(
            parent: animationController,
            curve: Curves.ease,
          ),
          child: Circle(
            size: 10,
            color: Colors.red[800],
          ),
        ),
      ),
    );
  }

  _next() {
    return Container(
      alignment: Alignment.bottomCenter,
      height: MediaQuery.of(context).size.height,
      margin: EdgeInsets.only(bottom: 70),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.6,
        child: RaisedButton(
          color: Colors.red[800],
          elevation: 0,
          padding: EdgeInsets.only(left: 55, right: 55),
          highlightElevation: 0,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          onPressed: () {
            if (currentPage == 3) {
              Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => LoginScreen(),
                  ));
            } else if (controller.page != 3) {
              controller.nextPage(
                  duration: kTabScrollDuration, curve: Curves.ease);
              if (currentPage != 3) {
                setState(() {
                  currentPage++;
                });
              }
            }
          },
          child: Text(currentPage != 3 ? "Próxima" : "Começar",
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                  fontSize: 16)),
        ),
      ),
    );
  }
}
