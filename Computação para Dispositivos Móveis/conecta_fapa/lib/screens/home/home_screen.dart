import 'package:conectafapa/models/category.dart';
import 'package:conectafapa/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:conectafapa/screens/feed/feed_screen.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';
import 'package:conectafapa/utils/content.dart' as content;

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  List<Category> categories;

  @override
  void initState() {
    super.initState();
    categories = content.categories();
  }

  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
      Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('./assets/icons/logo.png', width: 36),
                Text(
                  'Conecta FAPA',
                  style: TextStyle(
                      color: AppColors.prussianBlue,
                      fontSize: 15,
                      fontFamily: 'HindMedium',
                      height: 0.8),
                )
              ],
            ),
          ),
          Expanded(
            flex: 9,
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, childAspectRatio: 1.55),
                shrinkWrap: true,
                itemCount: categories.length - 1,
                itemBuilder: (BuildContext context, int index) {
                  return _cardItem(
                    categories[index + 1].image,
                    categories[index + 1].name,
                    () {
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          pageBuilder: (c, a1, a2) =>
                              FeedScreen(categories[index + 1].id),
                        ),
                      );
                    },
                  );
                }),
          )
        ],
      ),
      selected: 'Início',
    );
  }

  _cardItem(categoryImage, title, actionOnTap) {
    return GestureDetector(
      child: Container(
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(50))),
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover, image: AssetImage(categoryImage)),
                      borderRadius: BorderRadius.all(Radius.circular(15)))),
              Container(
                  decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0.0, 0.8, 0.9],
                  colors: [
                    Colors.transparent,
                    Colors.black45,
                    Colors.black54,
                  ],
                ),
              )),
              Padding(
                padding: EdgeInsets.only(bottom: 10, left: 20),
                child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 14,
                            height: 0.9,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            fontFamily: 'HindMedium'))),
              ),
            ],
          )),
      onTap: actionOnTap,
    );
  }
}
