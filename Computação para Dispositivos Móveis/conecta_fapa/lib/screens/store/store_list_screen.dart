import 'dart:async';
import 'package:conectafapa/screens/store/store_details_screen.dart';
import 'package:conectafapa/models/category_store.dart';
import 'package:conectafapa/models/store.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:conectafapa/utils/content.dart' as content;

class StoreListScreen extends StatefulWidget {
  @override
  State createState() => StoreListScreenState();
}

class StoreListScreenState extends State<StoreListScreen> {
  Completer<GoogleMapController> _controller = Completer();
  CategoryStore dropdownValue;
  CameraPosition _initialPosition;
  bool isLoading = true;
  List<Store> stores;
  List<CategoryStore> categoriesStores;
  TextEditingController searchController = TextEditingController();


  @override
  void initState() {
    _initialPosition = CameraPosition(
      target: LatLng(-30.0321453, -51.122529),
      zoom: 16.85,
    );
    stores = content.stores();
    categoriesStores = content.categoriesStores();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
        SingleChildScrollView(
            child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.35,
                  child: GoogleMap(
                    gestureRecognizers: Set()
                      ..add(Factory<PanGestureRecognizer>(
                          () => PanGestureRecognizer()))
                      ..add(Factory<ScaleGestureRecognizer>(
                          () => ScaleGestureRecognizer()))
                      ..add(Factory<TapGestureRecognizer>(
                          () => TapGestureRecognizer()))
                      ..add(Factory<VerticalDragGestureRecognizer>(
                          () => VerticalDragGestureRecognizer())),
                    mapType: MapType.normal,
                    initialCameraPosition: _initialPosition,
                    markers: Set<Marker>.of(stores.map((item) => Marker(
                        markerId: MarkerId('loja ${item.id}'),
                        position: LatLng(item.lat, item.long),
                        icon: BitmapDescriptor.defaultMarkerWithHue(
                            BitmapDescriptor.hueRed)))),
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  ),
                ),
                _inputSearch()
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Column(
                children: <Widget>[
                  _titleAndDropdown(),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: stores.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _cardItem(stores[index]);
                    },
                  )
                ],
              ),
            )
          ],
        )),
        selected: 'Lojas');
  }

  _inputSearch() {
    return Container(
      padding: EdgeInsets.all(20),
      height: 85,
      child: TextFormField(
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(17, 13, 17, 10),
            border: UnderlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.black, width: 0, style: BorderStyle.none),
                borderRadius: BorderRadius.circular(10)),
            filled: true,
            suffixStyle: TextStyle(color: Color.fromRGBO(194, 209, 217, 1)),
            hintText: 'Buscar...',
            hintStyle: TextStyle(fontFamily: 'HindMedium', height: 0.8),
            suffixIcon: Icon(IconData(59574, fontFamily: 'MaterialIcons')),
            fillColor: Colors.white),
        controller: searchController,
        onChanged: (value) {
          dropdownValue = null;
          stores = [];
          List<Store> temp = content.stores();
          temp.forEach((item) {
            if (item.name.toLowerCase().contains(value.toLowerCase())) {
              stores.add(item);
            }
          });
        },
      ),
    );
  }

  _titleAndDropdown() {
    return Padding(
      padding: EdgeInsets.only(
          top: 10, bottom: MediaQuery.of(context).size.height * 0.015),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Lojas e\nserviços',
                style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.06,
                    fontFamily: 'HindMedium',
                    height: 1.2,
                    color: Color.fromRGBO(0, 70, 97, 1))),
           Container(
                  decoration: BoxDecoration(color: Colors.transparent),
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: MediaQuery.of(context).size.height * 0.075,
                  padding: EdgeInsets.only(left: 13),
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Color.fromRGBO(122, 158, 171, 1)))),
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton<CategoryStore>(
                            icon: Icon(Icons.keyboard_arrow_down,
                                color: Color.fromRGBO(122, 158, 171, 1)),
                            hint: Text(
                              'Categoria',
                              style: TextStyle(
                                  color: Color.fromRGBO(0, 70, 97, 1),
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'HindMedium'),
                            ),
                            value: dropdownValue,
                            onChanged: (CategoryStore newValue) {
                              setState(() {
                                dropdownValue = newValue;
                              print(newValue.name);
                                searchController.clear();
                                stores = [];
                                List<Store> temp = content.stores();
                                temp.forEach((item) {
                                  if (item.category.id == newValue.id) {
                                    stores.add(item);
                                  }
                                });
                              });
                            },
                            items: categoriesStores
                                .map<DropdownMenuItem<CategoryStore>>(
                                    (CategoryStore value) {
                              return DropdownMenuItem<CategoryStore>(
                                value: value,
                                child: Text(value.name,
                                    style: TextStyle(
                                        color: Color.fromRGBO(0, 70, 97, 1),
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'HindMedium')),
                              );
                            }).toList())),
                  ))
        ],
      ),
    );
  }

  _cardItem(Store store) {
    return GestureDetector(
      child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(220, 220, 220, 0.2),
                  offset: Offset(0, 0),
                  blurRadius: 15,
                  spreadRadius: 5)
            ],
          ),
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 3,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: AspectRatio(
                            aspectRatio: 1.1,
                            child:
                                Image.asset(store.imageURL, fit: BoxFit.cover),
                          ))),
                  Expanded(
                    flex: 7,
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(store.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(0, 70, 97, 1),
                                  fontSize: 15,
                                  fontFamily: 'HindMedium',
                                  height: 0.8)),
                          Padding(
                              padding: EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(right: 3),
                                    child: Icon(Icons.location_on,
                                        size: 20,
                                        color:
                                            Color.fromRGBO(153, 153, 153, 1)),
                                  ),
                                  Expanded(
                                    child: Text(store.edifice,
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                127, 162, 176, 1),
                                            fontSize: 15,
                                            fontFamily: 'HindMedium',
                                            height: 1.7)),
                                  )
                                ],
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
      onTap: () {
        Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => StoreDetailsScreen(store),
            ));
      },
    );
  }
}
