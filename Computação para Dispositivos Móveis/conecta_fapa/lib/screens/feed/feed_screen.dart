import 'package:cached_network_image/cached_network_image.dart';
import 'package:conectafapa/models/category.dart';
import 'package:conectafapa/models/post.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:conectafapa/screens/feed/post_details_screen.dart';
import 'package:conectafapa/theme/app_colors.dart';
import 'package:conectafapa/theme/app_styles.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';
import 'package:conectafapa/widgets/posts_categories/posts_categories_list.dart';
import 'package:conectafapa/utils/content.dart' as content;

class FeedScreen extends StatefulWidget {
  int selectedCategory;

  FeedScreen(this.selectedCategory);

  @override
  FeedScreenState createState() {
    return new FeedScreenState();
  }
}

class FeedScreenState extends State<FeedScreen> {
  int _currentSelected = 1;
  List<Category> categories = [];
  List<Post> posts;

  @override
  void initState() {
    super.initState();
    categories = content.categories();
    _currentSelected = widget.selectedCategory;
    setPosts();
  }

  setPosts() {
    setState(() {
      posts = [];
      if (_currentSelected == 0) {
        posts = content.posts();
      } else {
        content.posts().forEach((Post item) {
          if (item.category.id == _currentSelected) {
            posts.add(item);
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
      SingleChildScrollView(
        child: Column(children: <Widget>[_categories(), _listPosts()]),
      ),
      selected: 'Posts',
      backgroundColor: AppColors.whiteSmoke,
    );
  }

  _categories() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      height: 120,
      color: AppColors.whiteSmoke,
      child: categories != null
          ? PostsCategoriesList(
              categories: categories,
              callback: (category) {
                setState(() {
                  widget.selectedCategory = category.id;
                  _currentSelected = category.id;
                  setPosts();
                });
              },
              currentSelected: _currentSelected)
          : Container(),
    );
  }

  _listPosts() {
    return ListView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemCount: posts.length,
      itemBuilder: (BuildContext context, int index) {
        return _post(posts[index]);
      },
    );
  }

  _post(Post post) {
    return post != null
        ? Padding(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 25),
            child: Container(
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: GestureDetector(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AspectRatio(
                        aspectRatio: 1.7,
                        child: CachedNetworkImage(
                          imageUrl: post.imageURL,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 12,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                              child: Text(
                                post.title,
                                style: AppStyles.customStyle(
                                    size: 16.9, height: 1.1),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 0, 20),
                        child: Text(
                          post.category.name,
                          style: TextStyle(
                              color: AppColors.astral,
                              fontFamily: 'HindMedium',
                              height: 0.8),
                          textAlign: TextAlign.left,
                        ),
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (c, a1, a2) => PostDetailsScreen(post),
                      ),
                    );
                  },
                ),
              ),
            ),
          )
        : Container();
  }
}
