import 'dart:ui';

class AppColors {
  static const Color whiteSmoke = Color.fromRGBO(246, 246, 246, 1);
  static const Color sherpaBlue = Color.fromRGBO(0, 70, 97, 1);
  static const Color pacificBlue = Color.fromRGBO(0, 153, 213, 1);
  static const Color astral = Color.fromRGBO(127, 162, 176, 1);
  static const Color aluminium = Color.fromRGBO(138, 140, 142, 1);
  static const Color prussianBlue = Color.fromRGBO(0, 68, 98, 1);
}
