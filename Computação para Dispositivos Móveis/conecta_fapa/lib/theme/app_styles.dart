import 'package:flutter/material.dart';
import 'package:conectafapa/theme/app_colors.dart';

class AppStyles {
  static const TextStyle defaultTextStyle = TextStyle(
      color: AppColors.sherpaBlue,
      fontWeight: FontWeight.w500,
      fontSize: 15,
      fontFamily: 'HindMedium',
      height: 0.8);

  static customStyle(
      {double size, double height, Color color, FontWeight fontWeight}) {
    return TextStyle(
        color: color != null ? color : AppColors.sherpaBlue,
        fontWeight: fontWeight != null ? fontWeight : FontWeight.w500,
        fontSize: size != null ? size : 15,
        fontFamily: 'HindMedium',
        height: height != null ? height : 0.8);
  }
}
