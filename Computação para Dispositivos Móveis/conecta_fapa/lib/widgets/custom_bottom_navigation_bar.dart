import 'dart:math' as math;

import 'package:conectafapa/screens/feed/feed_screen.dart';
import 'package:conectafapa/screens/store/store_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:conectafapa/screens/home/home_screen.dart';
import 'package:conectafapa/screens/menu/menu_screen.dart';
import 'package:conectafapa/theme/app_colors.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  Widget body;
  Color backgroundColor;
  String selected;

  CustomBottomNavigationBar(this.body, {this.selected, this.backgroundColor});

  @override
  CustomBottomNavigationBarState createState() {
    return CustomBottomNavigationBarState();
  }
}

class CustomBottomNavigationBarState extends State<CustomBottomNavigationBar>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          body: widget.body,
          backgroundColor: widget.backgroundColor != null
              ? widget.backgroundColor
              : AppColors.whiteSmoke,
          bottomNavigationBar: BottomAppBar(
            elevation: 0,
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: _openFromMenu(
                        Image.asset('./assets/icons/home.png',
                            height: 26, width: 26),
                        Image.asset('./assets/icons/home.png',
                            height: 26, width: 26, color: Colors.red[800]),
                        'Início',
                        HomeScreen()),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: _openFromMenu(
                        Image.asset('./assets/icons/post.png',
                            height: 26, width: 26),
                        Image.asset('./assets/icons/post.png',
                            height: 26, width: 26, color: Colors.red[800]),
                        'Posts',
                        FeedScreen(0)),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: _openFromMenu(
                        Image.asset('./assets/icons/store.png',
                            height: 26, width: 26),
                        Image.asset('./assets/icons/store.png',
                            height: 26, width: 26, color: Colors.red[800]),
                        'Lojas',
                        StoreListScreen()),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      child: _openFromMenu(
                          Image.asset('./assets/icons/menu.png',
                              height: 26, width: 26),
                          Image.asset('./assets/icons/menu.png',
                              height: 26, width: 26, color: Colors.red[800]),
                          'Menu',
                          MenuScreen())),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  open() {
    if (_animationController.isDismissed) {
      _animationController.forward();
    }
  }

  close() {
    if (_animationController.isCompleted) {
      _animationController.reverse();
    }
  }

  _openFromMenu(Image icon, Image activeIcon, String title, Widget screen) {
    return GestureDetector(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            widget.selected == title ? activeIcon : icon,
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                title,
                style: TextStyle(
                    fontFamily: 'HindMedium',
                    height: 0.8,
                    fontSize: 13,
                    color: widget.selected == title
                        ? Colors.red[800]
                        : AppColors.aluminium),
              ),
            )
          ],
        ),
        onTap: () {
          close();
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (c, a1, a2) => screen,
            ),
          );
        });
  }
}
