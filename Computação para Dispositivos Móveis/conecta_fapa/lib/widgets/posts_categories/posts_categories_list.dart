import 'package:conectafapa/models/category.dart';
import 'package:flutter/material.dart';
import 'package:conectafapa/widgets/posts_categories/posts_circle_button.dart';

class PostsCategoriesList extends StatefulWidget {
  List<Category> categories;
  void Function(dynamic) callback;
  int currentSelected;

  PostsCategoriesList(
      {@required this.categories,
      @required this.callback,
      this.currentSelected = 0})
      : assert(currentSelected != null);

  @override
  State<StatefulWidget> createState() {
    return new PostsCategoriesListState();
  }
}

class PostsCategoriesListState extends State<PostsCategoriesList>
    with AutomaticKeepAliveClientMixin<PostsCategoriesList> {
  ScrollController _scrollController =
      new ScrollController(initialScrollOffset: 0);

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  scrollToCategory() {
    for (int i = 0; i < widget.categories.length; i++) {
      if (widget.categories.elementAt(i).id == widget.currentSelected) {
        Future.delayed(Duration(milliseconds: 100), () {
          _scrollController.animateTo(i * 53.0,
              duration: new Duration(seconds: 2), curve: Curves.fastOutSlowIn);
        });
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {});
    scrollToCategory();
    return NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
          return true;
        },
        child: ListView(
            scrollDirection: Axis.horizontal,
            children: _buildList(),
            controller: _scrollController));
  }

  List<PostsCircleButton> _buildList() {
    var _list = List<PostsCircleButton>();
    for (var x = 0; x < widget.categories.length; x++) {
      _list.add(new PostsCircleButton(
        widget.categories[x].name,
        widget.currentSelected == widget.categories[x].id,
        () {
          widget.currentSelected = x;
          widget.callback(widget.categories[x]);
        },
        widget.categories[x].icon
      ));
    }
    return _list;
  }

  @override
  bool get wantKeepAlive => true;
}
