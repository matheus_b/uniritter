import 'package:conectafapa/models/category_store.dart';

class Store {
  int id;
  String name;
  String edifice;
  String description;
  CategoryStore category;
  String imageURL;
  double lat;
  double long;

  Store(this.id, this.name, this.edifice, this.description, this.category,
      this.imageURL, this.lat, this.long);
}
