import 'package:conectafapa/models/category.dart';

class Post extends Object {
  int id;
  String title;
  String imageURL;
  String date;
  String content;
  Category category;

  Post(this.id, this.title, this.imageURL, this.date, this.content,
      this.category);
}
