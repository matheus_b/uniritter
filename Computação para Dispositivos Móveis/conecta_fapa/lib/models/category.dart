class Category extends Object {
  int id;
  String name;
  String image;
  String icon;

  Category(this.id, this.name, this.image, this.icon);
}
