import 'package:comp_dispositivos_moveis_02/home_screen.dart';
import 'package:comp_dispositivos_moveis_02/signup_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  TextEditingController _emailController = TextEditingController(text: '');
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.1,
              MediaQuery.of(context).size.height * 0.08,
              MediaQuery.of(context).size.width * 0.1,
              MediaQuery.of(context).size.height * 0.08),
          child: Column(
            children: <Widget>[
              Text(
                'Login',
                style: TextStyle(fontSize: 22),
              ),
              _form(),
              Padding(
                padding: EdgeInsets.only(top: 30),
                child: Text(
                  'Ainda não tem uma conta?',
                  style: TextStyle(fontSize: 22),
                ),
              ),
              RaisedButton(
                child: Text('Crie uma agora'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignupScreen()));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  _form() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: _emailController,
            decoration: InputDecoration(hintText: 'E-mail'),
            validator: (value) => validateNotEmpty(value),
          ),
          TextFormField(
            decoration: InputDecoration(hintText: 'Senha'),
            obscureText: true,
            validator: (value) => validateNotEmpty(value),
          ),
          RaisedButton(
            child: Text('Enviar'),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            HomeScreen(_emailController.text)));
              }
            },
          )
        ],
      ),
    );
  }

  validateNotEmpty(value) {
    return value.isEmpty ? 'Campo obrigatório' : null;
  }
}
