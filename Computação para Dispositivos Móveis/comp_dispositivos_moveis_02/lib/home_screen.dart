import 'package:comp_dispositivos_moveis_02/signup_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  String name;

  HomeScreen(this.name);

  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.1,
              MediaQuery.of(context).size.height * 0.08,
              MediaQuery.of(context).size.width * 0.1,
              MediaQuery.of(context).size.height * 0.08),
          child: Column(
            children: <Widget>[
              Text(
                'Bem-vindo, ${widget.name}!',
                style: TextStyle(fontSize: 22),
              ),
              RaisedButton(
                child: Text('Sair'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignupScreen()));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
